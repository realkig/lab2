import random

class hash_map:
    def __setitem__(self, key, value):
        pass
    def __getitem__(self, key):
        pass
    def __resize(self):
        pass

class simple_hash_map(hash_map):
    def __init__(self):
        self.size = 0
        self.data = [None, None, None]
    
    def __hash(self, key, i):
        return (hash(key) + i) % len(self.data)
        
    def __find(self, key):
        i = 0;
        index = self.__hash(key, i);
        while self.data[index] != None and self.data[index][0] != key:
            i += 1
            index = self.__hash(key, i);
        return index;
    
    def __resize(self):
        temp = self.data
        self.data = [None] * (2*len(self.data) + 1)
        for item in temp:
            if item != None:
                self.data[self.__find(item[0])] = item
    
    def __setitem__(self, key, value):
        if self.size + 1 > len(self.data) // 2:
            self.__resize()
        index = self.__find(key)
        if self.data[index] == None:  
            self.size += 1
        self.data[index] = (key, value)
    
    def __getitem__(self, key):
        index = self.__find(key)
        if self.data[index] != None:
            return self.data[index][1]
        raise KeyError()

class random_hash_map(simple_hash_map):
    __rand_c = [5323]
    k = random.randint(1000, 999999)
    f = random.randint(1000, 999999)
    def __rand():
        l = len(self._rand_c) - 1
        while l < i:
            self.__rand_c.append((self.k * self.__rand_c[-1] + self.f) % 65546)
            l+=1
        return (self.k * self._rand_c[i] + self.f) % 65546
    def __hash(self, key, i):
        return (hash(key) + self._rand(i)) % len(self.data)

class chain_hash_map(hash_map):        
    def __init__(self):
        self.size = 0
        self.data = []
        self.__resize()
    
    def __hash(self, key):
        return hash(key) % len(self.data)
    
    def __insert(self, index, item):
        if self.data[index] == None:
            self.data[index] = [item]
            return True
        else:
            for i, t in enumerate(self.data[index]):
                if t[0] == item[0]:
                    self.data[index][i] = item
                    return False
            self.data[index].append(item)
            return True
    
    def __resize(self):
        temp = self.data
        self.data = [None] * (2*len(self.data) + 1)
        for bucket in temp:
            if bucket is not None:
                for key, value in bucket:
                    self.__insert(self.__hash(key), (key, value))
    
    def __setitem__(self, key, value):
        if self.size + 1 > len(self.data) // 1.5:
            self.__resize()
        if self.__insert(self.__hash(key), (key, value)):  
            self.size += 1
    
    def __getitem__(self, key):
        index = self.__hash(key)
        if self.data[index] != None:
            for k, v in self.data[index]:
                if k == key:
                    return v
        raise KeyError()

import datetime
now = datetime.datetime.now
rand = lambda: random.randint(10, 1000)

keys=[]
values=[]
l = (rand()) % 20 + 10


while len(keys) < l:
    d = 30 + (rand() % 300)
    while d in keys:
        d = 30 + (rand() % 300)
    keys.append(d)

k = l
l = range(l)

for i in l:
    values.append(50 + (rand() % 600))

s = now()
d = simple_hash_map()
for i in l:
    d[keys[i]] = values[i]
for i in l:
    print(keys[i], '=', d[keys[i]])
s = now() - s
print("Время выполнения (Метод простого рехеширования):", s)

s = now()
d = random_hash_map()
for i in l:
    d[keys[i]] = values[i]
for i in l:
    print(keys[i], '=', d[keys[i]])
s = now() - s
print("Время выполнения (Метод рехэширование с помощью псевдослучайных чисел):", s)

s = now()
d = chain_hash_map()
for i in l:
    d[keys[i]] = values[i]
for i in l:
    print(keys[i], '=', d[keys[i]])
s = now() - s
print("Время выполнения (Метод цепочек):", s)


