#include <iostream>
using std::cout;

void showBoard(int ** board, int SIZE)
{
    for(int a = 0; a < SIZE; ++a)
    {
        for(int b = 0; b < SIZE; ++b)
        {
            cout << ((board[a][b]==0) ? ". " : "Q ");
        }
        cout << '\n';
    }
}

bool tryQueen(int a, int b, int **board, int SIZE)
{
    SIZE-=1;
    int i, u;
    for (i =0; i<SIZE; i++){
        if (board[i][b] == 1){
            return false;
        }
        if (board[a][i] == 1){
            return false;
        }
    }
    i = a; u = b;
    while ((i < SIZE) & (u < SIZE)){
        i++;
        u++;
        if (board[i][u]==1){
            return false;
        }
    }
    i = a; u = b;
    while ((i > 0) & (u < SIZE)){
        i--;
        u++;
        if (board[i][u]==1){
            return false;
        }
    }
    i = a; u = b;
    while ((i < SIZE) & (u > 0)){
        i++;
        u--;
        if (board[i][u]==1){
            return false;
        }
    }
    i = a; u = b;
    while ((i > 0) & (u > 0)){
        i--;
        u--;
        if (board[i][u]==1){
            return false;
        }
    }
    return true;
}
 
int setQueen(int a, int ** board, int SIZE, int results_count = 0) 
{
    if(a == SIZE)
    {
        std::cout << "Результат №" << ++results_count << "\n";
        showBoard(board, SIZE);
        std::cout << "\n";
        return results_count; 
    }
    
    for(int i = 0; i < SIZE; ++i)
    {
        if(tryQueen(a, i, board, SIZE))
        {         
            board[a][i] = 1;
            results_count = setQueen(a+1, board, SIZE, results_count);
            board[a][i] = 0;
        }
    }
    return results_count; 
}

#define BOARD_SIZE 8
 
int main_task3()
{
    int ** board = new int * [BOARD_SIZE];
    for (int i =0; i<BOARD_SIZE; i++){
        board[i]=new int [BOARD_SIZE];
    }
    setQueen(0, board, BOARD_SIZE);
    return 0;
}


int main(int argv, char * args[])
{
   main_task3();
//  main_task3();
}
