#include <iostream>
#include <string>
#include <random>
#include <locale.h>
#include <bits/stdc++.h>

#include <chrono>


std::random_device rd;

int random_number(int min, int max){
    auto dist = new std::uniform_int_distribution<int> (min, max);
    return (int)dist[0](rd);
}

using std::cout;
using std::cin;
using std::string;

void print_array(int * data, int u){
    if (u<=0) cout << "[]";
    else{
        cout << "["<<data[0];
        for (int i = 1; i<u; i++) cout << ", "<<data[i];
        cout << "]";
    }
}

int * capture;
bool compareIndexes(int a, int b){
    return capture[a] < capture[b];
}

int * get_sorted_indexes(int * ar, int len){
    int i;
    int * out = new int[len];
    capture=ar;
    for (i = 0; i < len; i++) {
        out[i] = i;
    }
    std::sort(  out, out+len, compareIndexes );
    //cout<<"\n";
    //for (i = 0; i < len; i++) {
        //cout<<capture[out[i]];
        //cout<<"\n";
    //}
    return out;
}


struct Node{
    int key;
    Node * left = 0;
    Node * right = 0;
};

class Data{
    public:
    int * array;
    int * indexes;
    int length;
    Node * root = 0;
    bool is_sorted = false;
    bool is_rooted = false;
    Data(){
        root = new Node();
    }
    void sort(){
        if (!is_sorted)
        {
            this->indexes = get_sorted_indexes(this->array, this->length);
            is_sorted = true;
        };
    };
    void data_tree(){
        if (is_rooted) return;
        Node * node = root;
        int i = 0;
        while (i < length){
            node->key = i;
            i+=1;
            if (array[i]>array[node->key]){
                node->right = new Node();
                node = node->right;
            }
            else{
                node->left = new Node();
                node = node->left;
            }
        }
        is_rooted = true;
    }
};

class Fibonachi{
    public:
    int prev = 1;
    int next = 1;
    void step(){
        int j = prev;
        prev = next;
        next +=j;
    }
    void restart(){
        next = 1;
        prev = 1;
    }
};

Fibonachi fibonachi;


int binary_tree_search_recursion(int i, int * array, Node * node)
{
    recursion4:
    if (node == 0){
        return -1;
    }
    if (i == array[node->key]){
        return node->key;
    }
    else{
        if (i> array[node->key]){
            node = node->right;
        }
        else{
            node = node->left;
        }
        goto recursion4;
    }
    return -1;
}

int binary_tree_search(int i, Data data)
{
    data.data_tree();
    if (data.length == 0) return -1;
    return binary_tree_search_recursion(i, data.array, data.root);
}


int fibonachi_search_recursion(int i, int * array, int * indexes, int len)
{
    int temp, index;     
    recursion2:
    fibonachi.restart();
    index = indexes[fibonachi.next];
    temp = array[index];
    while (temp<i){
        fibonachi.step();
        if (fibonachi.next>len){
            index = array[len];
            temp = array[index];
            if (temp<i){
                return -1;
            }
            else if (temp == i){
                return index;
            }
            else{
                len -= fibonachi.prev;
                len += 1;
                indexes += fibonachi.prev;
                indexes -= 1;
                goto recursion2;
            }
        }
        index = indexes[fibonachi.next];
        temp = array[index];
    }
    if (temp > i){
        len = fibonachi.next - fibonachi.prev + 1;
        indexes+=fibonachi.prev;
        indexes-=1;
        goto recursion2;
    }
    if (temp == i){
        return index;
    }
    return 0;
}

int fibonachi_search(int i, Data data){
    data.sort();
    if (data.length == 0) return -1;
    return fibonachi_search_recursion(i, data.array, data.indexes-1, data.length);
};

int binary_search_recursion(int i, int * array, int * indexes, int begin, int end){
    int index, medium;    
    recursion1:
    if (begin>end) return -1;
    index = begin + ((end - begin)/2);
    medium = array[indexes[index]];
    if (medium < i){
        begin = index + 1;
        goto recursion1;
        // return binary_search_recursion(i, array, indexes, index+1, end);
    }
    else if (medium > i) {
        //return binary_search_recursion(i, array, indexes, begin, index-1);
        end = index - 1;
        goto recursion1;
    }
    return indexes[index];
}

int binary_search(int i, Data data){
    data.sort();
    if (data.length == 0) return -1;
    return binary_search_recursion(i, data.array, data.indexes, 0, data.length-1);
}

int interpol_search_recursion(int i, int * array, int * indexes, int left, int right)
{
    int mid;
    while (array[indexes[left]]<=i && array[indexes[right]]>=i)
    {
        mid=left+((i-array[indexes[left]])*(right-left))/(array[indexes[right]]-array[indexes[left]]);
        if (array[indexes[mid]]<i) left=mid+1; 
        else if (array[indexes[mid]]>i) right=mid-1;
        else return indexes[mid];
    }
    if (array[indexes[left]]==i) return indexes[left];
    else return -1;
}

int interpol_search(int i, Data data){
    data.sort();
    if (data.length == 0) return -1;
    return interpol_search_recursion(i, data.array, data.indexes, 0, data.length-1);    
};
#define recurse 10485
void check_function_runtime(int hole, Data data, string name, int (*search)(int, Data)){
    int result;
    int i = 0;
    
    cout<<"\nПоиск числа с помощью метода "<<name<<"\n";
    auto start = std::chrono::high_resolution_clock::now();
    while (i<recurse){
       result = binary_search(hole, data);
       i++;
    }
    auto finish = std::chrono::high_resolution_clock::now();
    cout<<"Индекс числа "<<hole<<" в массиве: "<<result<<"\n";    
    std::cout << "Время выполнения " << (float)(std::chrono::duration_cast<std::chrono::nanoseconds>(finish-start).count())/recurse << "ns\n";
}

int main()
{
    int i;
    
    int length = random_number(100, 200);
    int space = length * 2;
    int max = random_number(200, 100000);
    int min = random_number(0, (max>400000)?(max-400000):0);
    int * array = new int[space];
        
    for (i = 0; i < length; i++) array[i] = random_number(min, max);

    Data data;
    data.array = array;
    data.length = length;

    data.sort();
    data.data_tree();
    
    cout<<"Размер массива: "<<length<<"\n";
    cout<<"Массив: ";
    print_array(array, length);
    int hole;
    cout<<"\nВведите число, которое нужно найти: ";
    cin>>hole;
    
    check_function_runtime(hole, data, "\"Бинарное дерево поиска\"", binary_tree_search);
    check_function_runtime(hole, data, "\"Фибонначиев поиск\"", fibonachi_search);
    check_function_runtime(hole, data, "\"Интерполяционный поиск\"", interpol_search);
    check_function_runtime(hole, data, "\"Бинарный поиск\"", binary_search);
    return 0;
}

